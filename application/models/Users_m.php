<?php


class Users_m extends CI_Model 
{
    function __construct()
    {
        parent::__construct();
    }

    public function register($username, $password) {

        if ($username == "" || $password == "") {
            return false;
        } else {

            $this->db->where('username', $username);
            
            $query = $this->db->get('users');
            if ($query->num_rows() > 0 )
                return false;

            $arraySave = array(
                
                'username' => $username,
                'password' =>  password_hash($password,PASSWORD_BCRYPT),
                'account_type' => 'normal',
                'img' => 'img/2.jpg'
            );

            $result = $this->db->insert("users", $arraySave); // users table

            if ($result){
                return true;
            }
            else {
                return false;
            }
        }
    }

    
    public function login($username, $password) {
        if ($username == "" || $password == "") {
            return false;
        } else {
            $this->db->where('username', $username);
            $this->db->limit(1);
            $query = $this->db->get('users');

            if ($query->num_rows() > 0 ){
                $row = $query->row();
                if (password_verify($password, $row->password)) {
                    $data[] = array(
                        'iduser'=>$row->iduser,
                        'username'=>$row->username,
                        'img'=>$row->img,
                        'account_type'=>$row->account_type,
                        'created_at'=>$row->created_at

                    );
                    return $data;
                } else {
                    return false;
                }
            }
            else {
                return false;
            }
        }
    }
}


