<?php


class Playlists_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getallpl($iduser)
    {

        if ($iduser == "")
            return false;


        $this->db->where('iduser', $iduser);

        
        // $query = $this->db->query("SELECT * FROM playlists WHERE iduser='$iduser' and type='playlist");


        $query = $this->db->get('playlists');

        if ($query->num_rows() > 0) {


            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idpl' => $row->idpl,
                    'plname' => $row->plname,
                    'img' => $row->img,
                    'type' => $row->type,
                    'rating_' => $row->rating_
                ));
            }

            return $data;
        }
        return false;
    }



    public function getallsongsofpl($idpl)
    {
        if ($idpl == "")
            return false;

        $query = $this->db->query("SELECT * FROM songofpl sp, songs s WHERE sp.idpl='$idpl' and sp.idsong=s.idsong");

        if ($query->num_rows() > 0) {

            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'composer' => $row->composer,
                    'singer' => $row->singer,
                    'genre' => $row->genre,
                    'img' => $row->img,
                    'linkmp3' => $row->linkmp3,
                    'lyric' => $row->lyric,
                    'rating_' => $row->rating_,
                    'created_at' => $row->created_at,
                    'iduser' => $row->iduser

                ));
            }

            return $data;
        }
        return false;
    }


    public function getsonginfo($idsong)
    {
        if ($idsong == "")
            return false;

        $this->db->where('idsong', $idsong);
        $this->db->limit(1);
        $query = $this->db->get('songs');

        // $query = $this->db->query("SELECT * FROM songs WHERE idsong='$idsong'");

        if ($query->num_rows() > 0) {

            $data = array();
            $row = $query->row();
            // foreach ($query->result() as $row) {
                array_push($data, array(
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'composer' => $row->composer,
                    'singer' => $row->singer,
                    'genre' => $row->genre,
                    'img' => $row->img,
                    'linkmp3' => $row->linkmp3,
                    'rating_' => $row->rating_,
                    'created_at' => $row->created_at,
                    'iduser' => $row->iduser

                ));
            // }

            return $data;
        }
        return false;
    }

// khong su dung
     public function getsingerinfo($idsong)
    {
        if ($idsong == "")
            return false;

        $this->db->where('idsong', $idsong);
        $this->db->limit(1);
        $query = $this->db->get('songs');

        // $query = $this->db->query("SELECT * FROM songs WHERE idsong='$idsong'");

        if ($query->num_rows() > 0) {

            $data = array();
            $row = $query->row();
            // foreach ($query->result() as $row) {
                array_push($data, array(
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'singer' => $row->singer

                ));
            // }

            return $data;
        }
        return false;
    }

    
    public function getCommentsOfSong($idsong)
    {
        if ($idsong == "")
            return false;

        // $this->db->where('idsong', $idsong);
        // $this->db->limit(1);
        // $query = $this->db->get('songcomments');

        // $query = $this->db->query("SELECT * FROM songs WHERE idsong='$idsong'");
        $query = $this->db->query("SELECT * FROM songcomments sc, users u WHERE sc.idsong='$idsong' and sc.iduser=u.iduser"); //  LIMIT 50
        
        if ($query->num_rows() > 0) {

            $data = array();
            // $row = $query->row();
            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idscm' => $row->idscm,
                    'iduser' => $row->iduser,
                    'idsong' => $row->idsong,
                    'idsubscm' => $row->idsubscm,
                    'content' => $row->content,
                    'at_time' => $row->at_time,
                    'rating' => $row->rating,
                    'username' => $row->username,
                    'img' => $row->img

                ));
            }

            return $data;
        }
        return false;
    }

    
    public function sendCommentsOfSong($idsong,$iduser,$idsubscm, $content,$rating)
    {
        if ($idsong == "" || $iduser == "" || $idsubscm == "" || $content == "" || $rating == "")
            return false;

        // $query = $this->db->query("INSERT INTO `songcomments` (`idscm`, `iduser`, `idsong`, `idsubscm`, `content`, `at_time`, `rating`) VALUES (NULL, '$iduser', '$idsong', '$idsubscm', '$content', CURRENT_TIMESTAMP, '$rating');");
        
            if ($idsubscm == "NULL") {
                $arraySave = array(
                        
                    'iduser' => $iduser,
                    'idsong' => $idsong,
                    'content' => $content,
                    'rating' => $rating

                );
            } else {
                $arraySave = array(
                        
                    'iduser' => $iduser,
                    'idsong' => $idsong,
                    'idsubscm' => $idsubscm,
                    'content' => $content,
                    'rating' => $rating

                );
                
            }
        

        $result = $this->db->insert("songcomments", $arraySave); // users table

        if ($result){
            //update rating of song
            $query = $this->db->query("UPDATE songs, (SELECT idsong, FLOOR(AVG(rating)) as rating_ FROM `songcomments` WHERE rating <> -1 GROUP BY idsong) AS rated SET songs.rating_ = rated.rating_ WHERE songs.idsong=$idsong AND songs.idsong=rated.idsong");

            if ($query) {
                //update rating playlist
                $querypl = $this->db->query("UPDATE playlists, (SELECT idpl, FLOOR(AVG(songs.rating_)) as plrating_ FROM songofpl, songs WHERE songofpl.idsong=songs.idsong AND songs.rating_ <> -1  GROUP BY idpl) AS rated SET playlists.rating_=rated.plrating_ WHERE playlists.idpl=rated.idpl");

                if ($querypl) {
                    //xu ly gi do
                }
            }
            return true;
        }
        
        return false;
    }
    
    public function getCommentsOfPlaylist($idpl)
    {
        if ($idpl == "")
            return false;

        $query = $this->db->query("SELECT * FROM playlistcomments plc, users u WHERE plc.idpl='$idpl' and plc.iduser=u.iduser  LIMIT 15");
        
        if ($query->num_rows() > 0) {

            $data = array();
            // $row = $query->row();
            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idplcm' => $row->idplcm,
                    'iduser' => $row->iduser,
                    'idpl' => $row->idpl,
                    'idsubplcm' => $row->idsubplcm,
                    'content' => $row->content,
                    'at_time' => $row->at_time,
                    'rating' => $row->rating,
                    'username' => $row->username,
                    'img' => $row->img

                ));
            }

            return $data;
        }
        return false;
    }

 public function sendCommentsOfPlaylist($idpl, $iduser, $idsubplcm, $content, $rating)
    {
        if ($idpl == "" || $iduser == "" || $idsubplcm == "" || $content == "" || $rating == "")
            return false;

            if ($idsubplcm == "NULL") {
                $arraySave = array(
                        
                    'iduser' => $iduser,
                    'idpl' => $idpl,
                    'content' => $content,
                    'rating' => $rating

                );
            } else {
                $arraySave = array(
                        
                    'iduser' => $iduser,
                    'idpl' => $idpl,
                    'idsubplcm' => $idsubplcm,
                    'content' => $content,
                    'rating' => $rating

                );
                
            }
        

        $result = $this->db->insert("playlistcomments", $arraySave); // users table

        if ($result){
            return true;
        }
        
        return false;
    }

    
    public function getPlaylistOfUser($iduser)
    {
        if ($iduser == "")
            return false;

        $query = $this->db->query("SELECT pl.idpl , pl.plname , pl.img , pl.type , pl.rating_ , pl.status , pl.shared , pl.iduser , pl.created_at FROM playlists pl, users u WHERE pl.iduser='$iduser' and pl.iduser=u.iduser "); // and type='playlist'
        
        if ($query->num_rows() > 0) {

            $data = array();
            // $row = $query->row();
            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idpl' => $row->idpl,
                    'plname' => $row->plname,
                    'img' => $row->img,
                    'type' => $row->type,
                    'rating_' => $row->rating_,
                    'status' => $row->status,
                    'shared' => $row->shared,
                    'iduser' => $row->iduser,
                    'created_at' => $row->created_at

                ));
            }

            return $data;
        }
        return false;
    }

    public function getSongsOfPlaylist($idpl)
    {
        if ($idpl == "")
            return false;

        $query = $this->db->query("SELECT * FROM songofpl spl, songs s WHERE spl.idpl='$idpl' and spl.idsong=s.idsong");
        
        if ($query->num_rows() > 0) {

            $data = array();
            
            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idpl' => $row->idpl,
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'composer' => $row->composer,
                    'singer' => $row->singer,
                    'genre' => $row->genre,
                    'img' => $row->img,
                    'linkmp3' => $row->linkmp3,
                    'rating_' => $row->rating_,
                    'created_at' => $row->created_at,
                    'iduser' => $row->iduser,
                    'lyric' =>  $row->lyric

                ));
            }

            return $data;
        }
        return false;
    }

    public function collectPlaylistviewed($idpl,$iduser)
    {
        if ($idpl == "" || $iduser == "" )
            return false;

            if ($iduser == "NULL") {
                $arraySave = array(
                        
                    'idpl' => $idpl

                );
            } else {
                $arraySave = array(
                        
                    'iduser' => $iduser,
                    'idpl' => $idpl

                );
                
            }
        

        $result = $this->db->insert("playlistviewed", $arraySave); // users table

        if ($result){
            return true;
        }
        
        return false;
    }
    
    public function collectSongsplayed($idsong, $iduser)
    {
        if ($idsong == "" || $iduser == "" )
            return false;

        if ($iduser == "NULL") {
            $arraySave = array(
                    
                'idsong' => $idsong

            );
        } else {
            $arraySave = array(
                    
                'iduser' => $iduser,
                'idsong' => $idsong

            );
            
        }
    

        $result = $this->db->insert("songsplayed", $arraySave); // users table

        if ($result){
            return true;
        }
        
        return false;
    }

    
    public function searchAllPlaylists($value)
    {
        if ($value == "")
            return false;


        $query2 = $this->db->query("SELECT * FROM `playlists` WHERE plname LIKE '%$value%' LIMIT 15");
        
        $dataplaylist = array();

        if ($query2->num_rows() > 0) {

            foreach ($query2->result() as $row) {
                array_push($dataplaylist, array(
                    'idpl' => $row->idpl,
                    'plname' => $row->plname,
                    'img' => $row->img,
                    'type' => $row->type,
                    'rating_' => $row->rating_,
                    'status' => $row->status,
                    'shared' => $row->shared,
                    'iduser' => $row->iduser,
                    'created_at' => $row->created_at

                ));
            }

        }

        return $dataplaylist;

    }


    public function searchAllSongs($value)
    {
        if ($value == "")
            return false;


        $query1 = $this->db->query("SELECT * FROM `songs` WHERE songname LIKE '%$value%' or singer LIKE '%$value%' or composer LIKE '%$value%' or genre LIKE '%$value%' LIMIT 15");
        
        $datasong = array();

        if ($query1->num_rows() > 0) {

            foreach ($query1->result() as $row) {
                array_push($datasong, array(
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'composer' => $row->composer,
                    'singer' => $row->singer,
                    'genre' => $row->genre,
                    'img' => $row->img,
                    'linkmp3' => $row->linkmp3,
                    'rating_' => $row->rating_,
                    'created_at' => $row->created_at,
                    'iduser' => $row->iduser,
                    'lyric' =>  $row->lyric

                ));
            }

        }

        return $datasong;
    }

    public function insertPlaylist($iduser, $plname) {
        if ($plname == "" || $iduser == "" )
            return false;

            
        $arraySave = array(
                
            'iduser' => $iduser,
            'plname' => $plname,
            'img' => "img/1.jpg",
            'type' => "playlist",
            'status' => 0,
            'rating_' => -1,
            'shared' => 0,
            'status' => 0
            
        );
                
            
        

        $result = $this->db->insert("playlists", $arraySave); // users table

        if ($result){
            return true;
        }
        
        return false;
    }


    public function getPlaylistWithType($type)
    {
        if ($type == "" || $type == "playlist")
            return false;

        $query = $this->db->query("SELECT * FROM playlists  WHERE type='$type'");
        
        if ($query->num_rows() > 0) {

            $data = array();
            // $row = $query->row();
            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idpl' => $row->idpl,
                    'plname' => $row->plname,
                    'img' => $row->img,
                    'type' => $row->type,
                    'rating_' => $row->rating_,
                    'status' => $row->status,
                    'shared' => $row->shared,
                    'iduser' => $row->iduser,
                    'created_at' => $row->created_at

                ));
            }

            return $data;
        }
        return false;
    }

    public function removeSongInsidePlaylist($iduser, $idpl, $idsong)
    {
        if ($iduser == "" || $idpl == "" || $idsong == "")
            return false;

        $check = $this->db->query("SELECT idpl FROM playlists  WHERE iduser='$iduser' AND idpl=$idpl");
        if ($check->num_rows() <= 0) {
            return false;
        }

    
        $query = $this->db->query("DELETE FROM songofpl WHERE idpl=$idpl AND idsong=$idsong");
        
        if ($query) {
            return true;
        }
        return false;
    }

    
    public function insertTopic($plname, $type, $img) {
        if ($plname == "" || $type == ""  || $img == "" )
            return false;
 
        $arraySave = array(
                
            'plname' => $plname,
            'img' => $img,
            'type' => $type,
            'rating_' => -1,
            'shared' => 0,
            'iduser' => 1,
            'status' => 0
            
        );
                 
        $result = $this->db->insert("playlists", $arraySave);  

        if ($result){
            return true;
        }
        
        return false;
    }

    public function getPlaylistRecommend($iduser)
    {

        if ($iduser == "")
            return false;

        $query = $this->db->query("SELECT  p.idpl, p.plname, p.img, p.type, p.rating_, p.status, p.shared, p.iduser, p.created_at, A.solanplaylist FROM (SELECT pl.idpl, COUNT(plw.idplv) as 'solanplaylist' FROM playlists pl, playlistviewed plw, users u WHERE plw.iduser = u.iduser AND plw.idpl = pl.idpl AND plw.iduser=$iduser GROUP BY pl.idpl, plw.iduser ORDER BY COUNT(plw.idplv) DESC LIMIT 10) AS A , playlists p WHERE A.idpl=p.idpl");
        

        if ($query->num_rows() > 0) {


            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idpl' => $row->idpl,
                    'plname' => $row->plname,
                    'img' => $row->img,
                    'type' => $row->type,
                    'status' => $row->status,
                    'shared' => $row->shared,
                    'iduser' => $row->iduser,
                    'solanplaylist' => $row->solanplaylist,
                    'created_at' => $row->created_at,
                    'rating_' => $row->rating_
                ));
            }

            return $data;
        }
        return false;
    }

    
    public function removePlaylistOfUser($iduser, $idpl)
    {
        if ($iduser == "" || $idpl == "")
            return false;

        $check = $this->db->query("SELECT idpl FROM playlists  WHERE iduser='$iduser' AND idpl=$idpl");
        if ($check->num_rows() <= 0) {
            return false;
        }

    
        $query = $this->db->query("DELETE FROM playlistviewed WHERE idpl=$idpl");
        $query = $this->db->query("DELETE FROM playlistcomments WHERE idpl=$idpl");
        $query = $this->db->query("DELETE FROM songofpl WHERE idpl=$idpl");
        $query = $this->db->query("DELETE FROM playlists WHERE idpl=$idpl");
        
        if ($query) {
            return true;
        }
        return false;
    }

    
}

