<?php


class Songs_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getallcommentfakepl($iduser)
    {

        if ($iduser == "")
            return false;


        $this->db->where('iduser', $iduser);

        $query = $this->db->get('playlists');

        if ($query->num_rows() > 0) {


            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idpl' => $row->idpl,
                    'plname' => $row->plname,
                    'img' => $row->img,
                    'type' => $row->type,
                    'rating_' => $row->rating_
                ));
            }

            return $data;
        }
        return false;
    }

    public function importsongupload($songname, $singer, $composer, $genre, $linkmp3, $lyric, $img)
    {
        if ($songname == "" || $singer == "" || $composer == "" || $genre == "" || $linkmp3 == "" || $lyric == "" || $img == "")
            return false;

                 
        $arraySave = array(
                
            'songname' => $songname,
            'singer' => $singer,
            'composer' => $composer,
            'genre' => $genre,
            'linkmp3' => $linkmp3,
            'lyric' => $lyric,
            'img' => $img,
            'rating_' => -1
        );
        
        $result = $this->db->insert("songs", $arraySave); // users table

        if ($result){
            return true;
        }
        
        return false;
    }

    public function addsongintoplaylist($iduser, $idsong, $idpl)
    {

        if ($iduser == "" || $idsong == "" || $idpl == "")
            return false;


        $query = $this->db->query("SELECT sp.idpl FROM songofpl sp, playlists p WHERE sp.idpl=p.idpl and p.iduser=$iduser and sp.idsong=$idsong and p.idpl=$idpl");
    
        if ($query->num_rows() > 0) {

            return false;
        }

        $arraySave = array(                
            'idsong' => $idsong,
            'idpl' => $idpl 
        );


        $result = $this->db->insert("songofpl", $arraySave); 

        if ($result){
            return true;
        }
        return false;
    }

    

    public function getinfomusicdashboard()
    {

        $sobaihat=0;

        $query1 = $this->db->query("SELECT COUNT(*) AS sobaihat FROM `songs`");

        foreach ($query1->result() as $row) {
            $sobaihat = $row->sobaihat;
        }

  

        $soplaylist=0;

        $query2 = $this->db->query("SELECT COUNT(*) AS soplaylist FROM `playlists`");
 

        foreach ($query2->result() as $row) {
            $soplaylist = $row->soplaylist;
        }

    

        $playedinmonth=0;

        $query3 = $this->db->query("SELECT COUNT(*) AS playedinmonth FROM `songsplayed` WHERE EXTRACT(YEAR_MONTH FROM at_time) = EXTRACT(YEAR_MONTH FROM CURRENT_DATE())");
 
        foreach ($query3->result() as $row) {
            $playedinmonth = $row->playedinmonth;
        }

       

        $playedinmonthbefore=0;

        $query4 = $this->db->query("SELECT COUNT(*) AS playedinmonthbefore FROM `songsplayed` WHERE EXTRACT(YEAR_MONTH FROM at_time) = EXTRACT(YEAR_MONTH FROM (NOW() - INTERVAL 1 MONTH))");
 
        foreach ($query4->result() as $row) {
            $playedinmonthbefore = $row->playedinmonthbefore;
        }

     

        $data = array(
            'sobaihat' => $sobaihat,
            'soplaylist' => $soplaylist,
            'playedinmonth' => $playedinmonth,
            'playedinmonthbefore' => $playedinmonthbefore
        );

        return $data ;
    }

    
    public function getSongRecommend($iduser) 
    {

        if ($iduser == "")
            return false;

            // SELECT s.idsong, s.songname, s.composer, s.singer, s.genre, s.img, s.linkmp3,s.rating_,s.created_at,s.iduser, COUNT(*) as 'solannghe' FROM songsplayed sp, users u, songs s WHERE sp.iduser = u.iduser AND sp.idsong = s.idsong and u.iduser=1 GROUP BY s.idsong ORDER BY COUNT(*) DESC LIMIT 10
            
        $query = $this->db->query("SELECT s.idsong, s.songname, s.composer, s.singer, s.genre, s.img, s.linkmp3,s.rating_, s.created_at, s.iduser, s.lyric, COUNT(*) as 'solannghe' FROM songsplayed sp, users u, songs s WHERE sp.iduser = u.iduser AND sp.idsong = s.idsong and u.iduser=$iduser GROUP BY s.idsong ORDER BY COUNT(*) DESC LIMIT 10");

        if ($query->num_rows() > 0) {

            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'composer' => $row->composer,
                    'singer' => $row->singer,
                    'genre' => $row->genre,
                    'img' => $row->img,
                    'linkmp3' => $row->linkmp3,
                    'lyric' => $row->lyric,
                    'rating_' => $row->rating_,
                    'created_at' => $row->created_at,
                    'iduser' => $row->iduser,
                    'solannghe' => $row->solannghe

                ));
            }

            return $data;
        }
        return false;
    }

    public function getTopSongWeek()
    {
            
        $query = $this->db->query("SELECT s.idsong, s.songname, s.composer, s.singer, s.genre, s.img, s.linkmp3,s.rating_, s.created_at, s.iduser, s.lyric, COUNT(*) as 'solannghe' FROM songsplayed sp, songs s WHERE sp.idsong = s.idsong AND YEARWEEK(sp.at_time) = YEARWEEK(CURRENT_DATE()) GROUP BY s.idsong ORDER BY COUNT(*) DESC LIMIT 30");

        if ($query->num_rows() > 0) {

            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                    'idsong' => $row->idsong,
                    'songname' => $row->songname,
                    'composer' => $row->composer,
                    'singer' => $row->singer,
                    'genre' => $row->genre,
                    'img' => $row->img,
                    'linkmp3' => $row->linkmp3,
                    'lyric' => $row->lyric,
                    'rating_' => $row->rating_,
                    'created_at' => $row->created_at,
                    'iduser' => $row->iduser,
                    'solannghe' => $row->solannghe

                ));
            }

            return $data;
        }
        return false;
    }

    public function thongkeAllSongRating()
    {
 
        $query = $this->db->query("UPDATE songs, (SELECT idsong, FLOOR(AVG(rating)) as rating_ FROM `songcomments` WHERE rating <> -1 GROUP BY idsong) AS rated SET songs.rating_ = rated.rating_ WHERE songs.idsong=rated.idsong");

        if ($query) {
            return true;
        }

        return false;
    }

    
    public function statAllPlaylistRatingWithSongsRating()
    {
 
        $query = $this->db->query("UPDATE playlists, (SELECT idpl, FLOOR(AVG(songs.rating_)) as plrating_ FROM songofpl, songs WHERE songofpl.idsong=songs.idsong AND songs.rating_ <> -1  GROUP BY idpl) AS rated SET playlists.rating_=rated.plrating_ WHERE playlists.idpl=rated.idpl");

        if ($query) {
            return true;
        }
        
        return false;
    }
}



