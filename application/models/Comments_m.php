<?php


class Comments_m extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getsubcomment($idscm)
    {

        if ($idscm == "")
            return false;


        // $this->db->where('idsubscm', $idscm);

        $query = $this->db->query("SELECT * FROM songcomments sc, users u WHERE ( sc.idsubscm='$idscm' or sc.idscm='$idscm' ) and sc.iduser=u.iduser   LIMIT 50");

        if ($query->num_rows() > 0) {


            $data = array();

            foreach ($query->result() as $row) {
                array_push($data, array(
                     
                    'idscm' => $row->idscm,
                    'iduser' => $row->iduser,
                    'idsong' => $row->idsong,
                    'idsubscm' => $row->idsubscm,
                    'content' => $row->content,
                    'at_time' => $row->at_time,
                    'rating' => $row->rating,
                    'username' => $row->username,
                    'img' => $row->img
                ));
            }

            return $data;
        }
        return false;
    }


}

