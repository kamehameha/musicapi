<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	
	public $CI = NULL;

    public function __construct() {
        parent::__construct();
		$this->CI = & get_instance();
		
		$this->load->helper('url');
		
 		$this->load->library('session');
		$this->load->model('songs_m');
	}
	
	public function index()
	{
		 
		if ( $this->session->userdata('user')  === NULL) {
			// $user_id= $this->session->userdata('user');
			// if(!$user_id){
		
			
				// redirect('');
			$this->load->view('pages/login');
			// }
		}
		else {
			
	
			
			// $this->load->helper('url');

			// $this->load->view('welcome_message');

			$data = array();
			$data['title'] = "Trang Chủ";
			$data['thuvasai'] = "zxc Chủ";

			$musicinfo = $this->songs_m->getinfomusicdashboard();
			// 'sobaihat' => $sobaihat,
			// 'soplaylist' => $soplaylist,
			// 'playedinmonth' => $playedinmonth,
			// 'playedinmonthbefore' => $playedinmonthbefore
	
			$data['sobaihat'] = $musicinfo['sobaihat'];
			$data['soplaylist'] = $musicinfo['soplaylist'];
			$data['playedinmonth'] = $musicinfo['playedinmonth'];
			$data['playedinmonthbefore'] = $musicinfo['playedinmonthbefore'];
			
			$this->load->view('pages/trangchu', $data);
		}
		// $this->session->set_userdata('user', NULL);
 
		
	}

	// public function addtopic()
	// {
	
	// 	$this->load->view('pages/addtopic');
	// }
	// public function uploadsong()
	// {

		
	// 	$this->load->view('pages/uploadsong');
	// }
}

