<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadsong extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */


	public $CI = NULL;

    public function __construct() {
        parent::__construct();
		$this->CI = & get_instance();
		
		$this->load->library('session');
		$this->load->model('songs_m');
    }

	public function index()
	{
		if ( $this->session->userdata('user')  === NULL) {
			
			$this->load->view('pages/login');
			
		}
		else {
			$data['title'] = "Upload Song";
			$this->load->view('pages/uploadsong', $data);
		}
	}


	public function view()
	{
		$data['title'] = "Upload Song";
		$this->load->view('pages/uploadsong', $data);
	}

	// public function topicmanager()
	// {

	// 	$this->load->view('pages/topicmanager');
	// }

	public function douploadsong(){
		
		
    // Nếu người dùng click Upload
	if (isset($_POST['uploadSong']))
	{
		$flagUpload = true;

		$songname ="";
		$singer ="";
		$composer ="";
		$genre ="";
		$linkmp3 ="";
		$lyric ="";
		$img ="";
		

		if (isset($_POST['songname']))
		{
			$songname = $_POST['songname'];

		}
		else {
			$flagUpload = false;

		}


		if (isset($_POST['singer']))
		{
			$singer = $_POST['singer'];

		}
		else {
			$flagUpload = false;

		}

		if (isset($_POST['composer']))
		{
			$composer = $_POST['composer'];

		}
		else {
			$flagUpload = false;

		}

		if (isset($_POST['genre']))
		{
			$genre = $_POST['genre'];

		}
		else {
			$flagUpload = false;

		}

		// Nếu người dùng có chọn file để upload
		if (isset($_FILES['linkmp3']))
		{
			// Nếu file upload không bị lỗi,
			// Tức là thuộc tính error > 0
			if ($_FILES['linkmp3']['error'] > 0)
			{
				$flagUpload = false;
				echo 'File mp3 Upload Bị Lỗi-';
				
			}
			else{
				// Upload file
				$linkmp3 = 'general/' . $_FILES['linkmp3']['name'];
				move_uploaded_file($_FILES['linkmp3']['tmp_name'], './storage/'.$linkmp3);
				echo 'File mp3 Uploaded-';
			}
		}
		else{
			$flagUpload = false;
			echo 'Bạn chưa chọn file mp3 upload-';
			
		}

		if (isset($_FILES['lyric']))
		{
			// Nếu file upload không bị lỗi,
			// Tức là thuộc tính error > 0
			if ($_FILES['lyric']['error'] > 0)
			{
				$flagUpload = false;
				echo 'File lyric Upload Bị Lỗi-';
				
			}
			else{
				// Upload file
				$lyric = 'general/lyric/' . $_FILES['lyric']['name'];
				move_uploaded_file($_FILES['lyric']['tmp_name'], './storage/'.$lyric);
				echo 'File lyric Uploaded-';
			}
		}
		else{
			$flagUpload = false;
			echo 'Bạn chưa chọn file lyric upload-';
			
		}

		if (isset($_FILES['img']))
		{
			// Nếu file upload không bị lỗi,
			// Tức là thuộc tính error > 0
			if ($_FILES['img']['error'] > 0)
			{
				$flagUpload = false;
				echo 'File img Upload Bị Lỗi';
				
			}
			else{
				// Upload file
				$img = 'img/' . $_FILES['img']['name'];
				move_uploaded_file($_FILES['img']['tmp_name'], './storage/'.$img);
				echo 'File img Uploaded-';
			}
		}
		else{
			$flagUpload = false;
			echo 'Bạn chưa chọn file img upload-';
			
		}

		// echo "<br>" . $songname . "<br>";
		// echo $singer . "<br>";
		// echo $composer  . "<br>";
		// echo $genre  . "<br>";
		// echo $linkmp3  . "<br>";
		// echo $lyric  . "<br>";
		// echo $img . "<br>";

		if ($flagUpload) {
			$result = $this->songs_m->importsongupload($songname, $singer, $composer, $genre, $linkmp3, $lyric, $img);
			if ($result) {
				echo "<br>Upload Song Thanh Cong<br>";
			}
			else {
				echo "<br>Upload Song That Bai<br>";
			}
		}
		else {
			echo "Upload that bai";
		}
	}
	}
}
