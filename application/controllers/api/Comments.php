<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Comments extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('comments_m');
    }

    public function getsubcomment_post()
    {
        $idscm = $this->post('idscm');


        $comments = $this->comments_m->getsubcomment($idscm);

        if ($comments) {
            $this->response(array('status' => true, 'message' => 'getsubcomment success', 'response' => $comments), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getsubcomment error.'), 200);
        }
    }


}
