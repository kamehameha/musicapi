<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Songs extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }

    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('songs_m');
    }

    public function getallpl_post()
    {
        $iduser = $this->post('iduser');


        $songs = $this->songs_m->getallcommentfakepl($iduser);

        if ($songs) {
            $this->response(array('status' => true, 'message' => 'getallpl success', 'response' => $songs), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getallpl error.'), 200);
        }
    }

    public function addsongintoplaylist_post()
    {
        $iduser = $this->post('iduser');
        $idsong = $this->post('idsong');
        $idpl = $this->post('idpl');

        $songs = $this->songs_m->addsongintoplaylist($iduser, $idsong, $idpl);

        if ($songs) {
            $this->response(array('status' => true, 'message' => 'addsongintoplaylist success', 'response' => $songs), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'addsongintoplaylist error.'), 200);
        }
    }

    
    public function getSongRecommend_post()
    {
        $iduser = $this->post('iduser');
        
        $songs = $this->songs_m->getSongRecommend($iduser);

        if ($songs) {
            $this->response(array('status' => true, 'message' => 'getSongRecommend success', 'response' => $songs), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getSongRecommend error.'), 200);
        }
    }
    
    public function getTopSongWeek_post()
    {
        
        
        $songs = $this->songs_m->getTopSongWeek();

        if ($songs) {
            $this->response(array('status' => true, 'message' => 'getTopSongWeek success', 'response' => $songs), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getTopSongWeek error.'), 200);
        }
    }
}
