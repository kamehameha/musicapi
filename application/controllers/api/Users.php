<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Users extends CI_Controller  {
    use REST_Controller { REST_Controller::__construct as private __resTraitConstruct; }
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct(); 

        $this->load->model('users_m');
    }

    public function register_post() {
        $username = $this->post('username');
        $password = $this->post('password');
        
        $registration = $this->users_m->register($username, $password);
        if ($registration) {
            $this->response(array('status'=>$registration, 'message'=>'register success'), 200);
        }
        else {
            $this->response(array('status'=>$registration, 'message'=>'register error. Hung'), 200);

        }

    }

    public function login_post() {
        $username = $this->post('username');
        $password = $this->post('password');
        
        $login = $this->users_m->login($username, $password);
        if (!$login) {
            $this->response(array('status'=>false, 'message'=>'login error'), 200);
        }
        else {
            $this->response(array('status'=>true, 'message'=>'login success.', 'response'=> $login), 200);

        }

    }
}
