<?php

use Restserver\Libraries\REST_Controller;

defined('BASEPATH') or exit('No direct script access allowed');


require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Playlists extends CI_Controller
{
    use REST_Controller {
        REST_Controller::__construct as private __resTraitConstruct;
    }
    function __construct()
    {
        // Construct the parent class
        parent::__construct();
        $this->__resTraitConstruct();

        $this->load->model('playlists_m');
    }

    public function getallpl_post()
    {
        $iduser = $this->post('iduser');


        $playlists = $this->playlists_m->getallpl($iduser);

        if ($playlists) {
            $this->response(array('status' => true, 'message' => 'getallpl success', 'response' => $playlists), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getallpl error.'), 200);
        }
    }

    public function getallpl_get()
    {
        $iduser = $this->get('iduser');


        $playlists = $this->playlists_m->getallpl($iduser);

        if ($playlists) {
            $this->response(array('status' => true, 'message' => 'getallpl success', 'response' => $playlists), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getallpl error.'), 200);
        }
    }

    
    public function getallsongsofpl_post()
    {
        $idpl = $this->post('idpl');


        $songs = $this->playlists_m->getallsongsofpl($idpl);

        if ($songs) {
            $this->response(array('status' => true, 'message' => 'getallpl success', 'response' => $songs), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getallpl error.'), 200);
        }
    }

    
    public function getsonginfo_post()
    {
        $idsong = $this->post('idsong');


        $song = $this->playlists_m->getsonginfo($idsong);

        if ($song) {
            $this->response(array('status' => true, 'message' => 'getsonginfo_ success', 'response' => $song), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getsonginfo_ error.'), 200);
        }
    }

    public function getCommentsOfSong_post()
   {
        $idsong = $this->post('idsong');


        $song = $this->playlists_m->getCommentsOfSong($idsong);

        if ($song) {
            $this->response(array('status' => true, 'message' => 'getsonginfo_ success', 'response' => $song), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getsonginfo_ error.'), 200);
        }
    }


    public function sendCommentsOfSong_post()
   {
        $idsong = $this->post('idsong');
        $iduser = $this->post('iduser');
        $idsubscm = $this->post('idsubscm');
        $content = $this->post('content');
        $rating = $this->post('rating');
        


        $result = $this->playlists_m->sendCommentsOfSong($idsong,$iduser,$idsubscm, $content,$rating);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'sendCommentsOfSong success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'sendCommentsOfSong error.'), 200);
        }
    }

    // POST: lấy các comment của playlist 
    public function getCommentsOfPlaylist_post()
   {
        $idpl = $this->post('idpl');


        $result = $this->playlists_m->getCommentsOfPlaylist($idpl);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'getCommentsOfPlaylist success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getCommentsOfPlaylist error.'), 200);
        }
    }
  
    // POST: gửi bình luận playlist
    public function sendCommentsOfPlaylist_post()
   {
        $idpl = $this->post('idpl');
        $iduser = $this->post('iduser');
        $idsubplcm = $this->post('idsubplcm'); // send "NULL" if empty
        $content = $this->post('content');
        $rating = $this->post('rating');
        
        $result = $this->playlists_m->sendCommentsOfPlaylist($idpl,$iduser,$idsubplcm, $content,$rating);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'sendCommentsOfPlaylist success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'sendCommentsOfPlaylist error.'), 200);
        }
    }
    
    
    public function getPlaylistOfUser_post()
   {
        $iduser = $this->post('iduser');


        $result = $this->playlists_m->getPlaylistOfUser($iduser);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'getPlaylistOfUser_post success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getPlaylistOfUser_post error.'), 200);
        }
    }
    
    public function getSongsOfPlaylist_post()
   {
        $idpl = $this->post('idpl');


        $result = $this->playlists_m->getSongsOfPlaylist($idpl);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'getSongsOfPlaylist_post success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'getSongsOfPlaylist_post error.'), 200);
        }
    }


    public function collectPlaylistviewed_post()
   {
        $idpl = $this->post('idpl');
        $iduser = $this->post('iduser'); // "NULL" neu khong co user


        $result = $this->playlists_m->collectPlaylistviewed($idpl,$iduser);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'collectPlaylistviewed success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'collectPlaylistviewed error.'), 200);
        }
    }

    
    public function collectSongsplayed_post()
   {
        $idsong = $this->post('idsong');
        $iduser = $this->post('iduser'); // "NULL" neu khong co user


        $result = $this->playlists_m->collectSongsplayed($idsong,$iduser);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'collectSongsplayed success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'collectSongsplayed error.'), 200);
        }
    }

    
    public function searchAllSource_post()
   {
        
        $value = $this->post('value'); 


        $result1 = $this->playlists_m->searchAllSongs($value);
        $result2 = $this->playlists_m->searchAllPlaylists($value);

        if ($result1 || $result2) {
            $this->response(array('status' => true, 'message' => 'searchAllSource success', 'response' => array(

                'songs' => $result1,
                'playlists' => $result2

            )), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'searchAllSource error.'), 200);
        }
    }


    public function insertPlaylist_post()
   {
        $iduser = $this->post('iduser');
        $plname = $this->post('plname');


        $result = $this->playlists_m->insertPlaylist($iduser, $plname);

        if ($result) {
            $this->response(array('status' => true, 'message' => 'insertPlaylist success', 'response' => $result), 200);
        } else {
            $this->response(array('status' => false, 'message' => 'insertPlaylist error.'), 200);
        }
    }

    public function getPlaylistWithType_post()
    {
         $type = $this->post('type');
 
 
         $result = $this->playlists_m->getPlaylistWithType($type);
 
         if ($result) {
             $this->response(array('status' => true, 'message' => 'getPlaylistWithType success', 'response' => $result), 200);
         } else {
             $this->response(array('status' => false, 'message' => 'getPlaylistWithType error.'), 200);
         }
     }
     
     public function removeSongInsidePlaylist_post()
     {
          $iduser = $this->post('iduser');
          $idpl = $this->post('idpl');
          $idsong = $this->post('idsong');
  
  
          $result = $this->playlists_m->removeSongInsidePlaylist($iduser, $idpl, $idsong);
  
          if ($result) {
              $this->response(array('status' => true, 'message' => 'removeSongInsidePlaylist success', 'response' => $result), 200);
          } else {
              $this->response(array('status' => false, 'message' => 'removeSongInsidePlaylist error.'), 200);
          }
      }

      
     public function getPlaylistRecommend_post()
     {
          $iduser = $this->post('iduser'); 
   
          $result = $this->playlists_m->getPlaylistRecommend($iduser );
  
          if ($result) {
              $this->response(array('status' => true, 'message' => 'getPlaylistRecommend success', 'response' => $result), 200);
          } else {
              $this->response(array('status' => false, 'message' => 'getPlaylistRecommend error.'), 200);
          }
      }
     
      public function removePlaylistOfUser_post()
      {
           $iduser = $this->post('iduser');
           $idpl = $this->post('idpl');
   
   
           $result = $this->playlists_m->removePlaylistOfUser($iduser, $idpl);
   
           if ($result) {
               $this->response(array('status' => true, 'message' => 'removePlaylistOfUser success', 'response' => $result), 200);
           } else {
               $this->response(array('status' => false, 'message' => 'removePlaylistOfUser error.'), 200);
           }
       }
}
