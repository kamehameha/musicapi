<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Addtopic extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	 
	public $CI = NULL;

    public function __construct() {
        parent::__construct();
		$this->CI = & get_instance();
		
		$this->load->library('session');
		$this->load->model('playlists_m');
    }


	public function index()
	{
		
		if ( $this->session->userdata('user')  === NULL) {
			
			$this->load->view('pages/login');
			
		}
		else {
			$data['title'] = "Thêm mới chủ đề";
			$this->load->view('pages/addtopic', $data);
		}
	}

	public function doAddtopic()
	{
		if (isset($_POST['addTopic']))
		{
			$flagUpload = true;
			$plname = "";
			$type = "";
			$img = "";

			if (isset($_POST['plname']))
			{
				$plname = $_POST['plname'];

			}
			else {
				$flagUpload = false;

			}

			if (isset($_POST['type']))
			{
				$type = $_POST['type'];

			}
			else {
				$flagUpload = false;

			}

			if (isset($_FILES['img']))
			{
				// Nếu file upload không bị lỗi,
				// Tức là thuộc tính error > 0
				if ($_FILES['img']['error'] > 0)
				{
					$flagUpload = false;
					echo 'File img Upload Bị Lỗi';
					
				}
				else{
					// Upload file
					$img = 'img/' . $_FILES['img']['name'];
					move_uploaded_file($_FILES['img']['tmp_name'], './storage/'.$img);
					echo 'File img Uploaded-';
				}
			}
			else{
				$flagUpload = false;
				echo 'Bạn chưa chọn file img upload-';
				
			}

			echo "$plname - $type - $img";

			if ($flagUpload) {
				$result = $this->playlists_m->insertTopic($plname, $type, $img );
				
				if ($result) {
					echo "<br>Thêm mới danh sách nhạc Thành Công<br>";
				}
				else {
					echo "<br>Thêm mới danh sách nhạc Thất bại<br>";
				}
			}
			else {
				echo "Thêm mới danh sách nhạc Thất bại";
			}

		}
	}
}
