<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('pages/header');    ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php $this->load->view('pages/sidebar');    ?> 
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->

    
    <div id="content-wrapper" class="d-flex flex-column">

<!-- Main Content -->
<div id="content">

  <!-- Begin Page Content -->
<div class="container-fluid">

    <form action="" method="POST" enctype="multipart/form-data" id="formaddtopic"  onsubmit="return Validate(this);">
        <div class="form-group">
          <label for="txtplnam1">Tên chủ đề:</label>
          <input type="text" class="form-control" id="txtplname" name="plname" required>
        </div>
        <div class="form-group">
          <label for="selecttopic">Chọn loại cho danh sách</label>
          <select class="form-control" id="selecttopic" name="type">
            <option value="topic">topic</option>
            <option value="hottrend">hottrend</option>
            <option value="topsong">topsong</option>
            <option value="hotchill">hotchill</option>
            <option value="hotcomment">hotcomment</option>
          </select>
        </div>
        <br>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile1" name="img" required>
            <label class="custom-file-label" for="customFile1">Chọn hình dại diện chủ đề</label>
          </div>
         
          <br>
          <div class="errorTxt"  ></div>
          <br>
          
      <button type="submit" class="btn btn-primary" name="addTopic">Submit</button>
        </form>

  <?php 
    // Xử Lý Upload
    $this->CI->doAddtopic();
    
  ?>
</div>
  <!-- /.container-fluid -->

</div>
<!-- End of Main Content -->

<!-- End of Main Content -->

<!-- Footer -->
<footer class="sticky-footer bg-white">
  <div class="container my-auto">
    <div class="copyright text-center my-auto">
      <span>Copyright &copy; Your Website 2019</span>
    </div>
  </div>
</footer>
<!-- End of Footer -->

</div>





    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Footer -->
  <?php $this->load->view('pages/footer');    ?> 


</body>
<script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });

  function hasExtension(inputID, exts) {
        var fileName = document.getElementById(inputID).value;
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }

    function Validate(oForm) {
  
    
      if (!hasExtension('customFile', ['.jpg', 'png'])) {
        alert("File nhac sai dinh dang (jpg, png)");
          return false;
      }
        return true;
    }
    
 

  </script>
</html>
