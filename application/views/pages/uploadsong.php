<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('pages/header');    ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php $this->load->view('pages/sidebar');    ?> 
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->

    
    <div id="content-wrapper" class="d-flex flex-column">

      <!-- Main Content -->
      <div id="content">

        <!-- Begin Page Content -->
      <div class="container-fluid" ">
<!-- ?php echo base_url().""; ?/action_page.php -->
      <form action="" method="POST" enctype="multipart/form-data" id="formuploadsong"  onsubmit="return Validate(this);">
        <div class="form-group">
          <label for="usr">Tên bài hát:</label>
          <input type="text" class="form-control" id="usr" name="songname" required>
        </div>
        <div class="form-group">
          <label for="usr">Ca sĩ:</label>
          <input type="text" class="form-control" id="usr" name="singer" required>
        </div>
        <div class="form-group">
          <label for="usr">Nhạc sĩ:</label>
          <input type="text" class="form-control" id="usr" name="composer" required>
        </div>
        <div class="form-group">
          <label for="usr">Thể loại:</label>
          <input type="text" class="form-control" id="usr" name="genre" required>
        </div>
        <br>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile1" name="linkmp3" required>
            <label class="custom-file-label" for="customFile1">Choose file mp3 song</label>
          </div>
          <br>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile2" name="lyric" required>
            <label class="custom-file-label" for="customFile2">Choose file lyric</label>
          </div>
          <br>
          <div class="custom-file">
            <input type="file" class="custom-file-input" id="customFile3" name="img" required>
            <label class="custom-file-label" for="customFile3">Choose file image song</label>
          </div>

          <br>
          <div class="errorTxt" style="font-size:1rem;"></div>
          <br>
          
  <button type="submit" class="btn btn-primary" name="uploadSong">Submit</button>
        </form>
  <?php 
    // Xử Lý Upload
    $this->CI->douploadsong();
    
  ?>

      </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright &copy; Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->

</div>





    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->


  <!-- Footer -->
  <?php $this->load->view('pages/footer');    ?> 
 
        

</body>
  <script>
  // Add the following code if you want the name of the file appear on select
  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
  });
  function hasExtension(inputID, exts) {
        var fileName = document.getElementById(inputID).value;
        return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(fileName);
    }

    function Validate(oForm) {
      if (!hasExtension('customFile1', ['.mp3'])){
        alert("File nhac sai dinh dang (mp3)");
          return false;
      }
        
      if (!hasExtension('customFile2', ['.vtt']) ){
        alert("File nhac sai dinh dang (vtt)");
          return false;

      }
      
      
    
      if (!hasExtension('customFile3', ['.jpg', 'png'])) {
        alert("File nhac sai dinh dang (jpg, png)");
          return false;
      }
        return true;
    }
    
  $(document).ready(function() {
 
 //Khi bàn phím được nhấn và thả ra thì sẽ chạy phương thức này
//  $("#formuploadsong").validate({
//      rules: {
//       songname: "required",
//       singer: "required",
//       composer: "required",
//       genre: "required",
//       linkmp3:"required",
//       lyric:"required",
//       img:"required"
//      },
//      messages: {
//       songname: "Vui lòng nhập Tên bài hát",
//       singer: "Vui lòng nhập Tên ca sĩ",
//       composer: "Vui lòng nhập Tên tác giả",
//       genre: "Vui lòng nhập Thể loại",
//       linkmp3: {
//              required: "Vui lòng nhập file mp3"
//          },
//       lyric: {
//              required: "Vui lòng nhập file lyric(.vtt)"
//          },
//       img: {
//              required: "Vui lòng nhập file image"
//          }
//      }

//  });

    // $('#formuploadsong').on('submit', function(e){
    //     // validation code here
    //     valid = true;
        
    //     if(!valid) {
    //       e.preventDefault();
    //     }
    //   });

  
   
});

  </script>
</html>
