<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>

<!DOCTYPE html>
<html lang="en">

<?php $this->load->view('pages/header');    ?>

<body id="page-top">

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->

    <?php $this->load->view('pages/sidebar');    ?> 
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
  
    <?php
      $dashboarddata = array();
      $dashboarddata['numsongs'] = $sobaihat;
      $dashboarddata['numplaylists'] = $soplaylist;
      $dashboarddata['luotnghettrc'] = $playedinmonthbefore;
      $dashboarddata['luotnghe'] = $playedinmonth;
      $this->load->view('pages/dashboard', $dashboarddata);    
    ?> 
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Footer -->
  <?php $this->load->view('pages/footer');    ?> 


</body>

</html>
